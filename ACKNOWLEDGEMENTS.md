# Acknowledgements

This is a list of people who are not the project lead and who made tangible
contributions to the code base.

## Artwork

* Frog logo by ziq.

## Translations

* Dutch translation by Xesau.

* Esperanto translation by Jade.

* German translation by evilsjw.

* Greek translation contributed by ziq.

* Portuguese (Brazilian) translation contributed by VitorVRS.

* Spanish translation contributed by sancocho.
